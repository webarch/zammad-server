#!/usr/bin/env bash

if [[ "${2}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook root.yml --limit "${1}" --tags "${2}"
elif [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook root.yml --limit "${1}"
else
  ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook root.yml
fi
