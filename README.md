# Zammad Server

This repo contains Ansible to provision a development Zammad site running on
Debian Buster.

To setup your own server copy this repo and then edit the
[hosts.yml](hosts.yml) file to change `zammad.webarch.net` to the name of your
server and then move and edit the
[host_vars/zammad.webarch.net.yml](host_vars/zammad.webarch.net.yml) file.

The required Ansible roles are listed in the
[requirements.yml](requirements.yml) file, the key one being the [Zammad
role](https://git.coop/webarch/zammad), the variables for this role are in the
[hosts.yml](hosts.yml) file and the variables for the other roles are in the
[group_vars/zammad_servers.yml](group_vars/zammad_servers.yml) file.

## Update

To run all the roles you can simply use [this Bash
script](https://git.coop/webarch/zammad-server/-/blob/master/zammad.sh):

```bash
./zammad.sh
```

To only run the [zammad role](https://git.coop/webarch/zammad) you can use the
Bash script like this:

```bash
./zammad.sh zammad
```

## Build

After creating a virtual server Python needs to be installed and also SSH keys
need to be added for the root user, this can be done using the console, for
example:

```bash
apt install -y python3 ssh-import-id && \
ssh-import-id chriscroome
```

### Account creation

Run this script to `ssh` to the server as `root` and add the sudo accounts:

```bash
./root.sh
```

### Install

Run this script to install and update Zammad:

```bash
./zammad.sh
```

### Configure Zammad

Visit the server address using a web browser and configure Zammad using the web interface.


