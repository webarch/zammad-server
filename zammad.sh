#!/usr/bin/env bash

if [[ "${1}" != "" ]]; then
  ansible-galaxy install -r requirements.yml --force && \
  ansible-playbook zammad.yml --tags "${1}"
else
  ansible-galaxy install -r requirements.yml --force && \
  ansible-playbook zammad.yml
fi

